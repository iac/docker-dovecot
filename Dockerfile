FROM alpine:3

RUN apk add --no-cache tini ca-certificates dovecot dovecot-ldap dovecot-lmtpd dovecot-pigeonhole-plugin dovecot-pop3d python3 ssmtp && \
    addgroup -g 1000 mailuser0 && adduser -D -u 1000 -G mailuser0 mailuser0

ADD dovecot.conf /etc/dovecot/dovecot.conf
ADD ssmtp.conf /etc/ssmtp/ssmtp.conf

VOLUME ["/etc/dovecot", "/srv/mail"]
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/sbin/dovecot", "-F"]
